# Open Access Control API

This Ruby script listens to a USB serial port, parses console input, and parses serial information from the Open Access Control v4 (software 1.4) board.

Right now it just does a 1:1 translation, later it will implement a JSON API.
